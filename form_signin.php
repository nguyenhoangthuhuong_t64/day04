<?php
echo "<!DOCTYPE html>
<html lang='en'>

<head>
    <meta charset='UTF-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <title>Document</title>

    <style>

        .required:after {
            color: #FF3939;
            content: ' *';
        }

        #body {
            width: 390px;
            height: 430px;
            border: solid #547FA5 2px;
            padding: 20px 40px 10px 20px;
        }

        .gender {
            margin: 0px 10px 0px 15px;
        }

        .frames {
            color: black;
            border: solid #41719C 2px;
            height: 30px;
            width: 80%;
        }

        table {
            border-collapse: separate;
            border-spacing: 15px 15px;
        }

        label {
            font-size: 14px;
            font-family: 'Times New Roman', Times, serif;
        }

        .background_blue {
            color: aliceblue;
            background-color: #70AD47;
            padding: 10px 10px 0px 10px;
            border: solid #41719C 2px;
        }

        .blue-box {
            border: #41719C solid 2px;
            line-height: 20px;
            height: 30px;
            width: 80%;
        }

        .submit {
            background-color: #70AD47;
            color: aliceblue;
            justify-content: center;
            padding: 10px 20px 10px 20px;
            width: 30%;
            border-radius: 7px;
            font-family: 'Times New Roman', Times, serif;
            font-size: 14px;
            border: solid #41719C 2px;
            margin: 5% 0% 0% 40%;
        }

        .error {
            color: red;
            font-size: 14px;
            margin-left: 20px;
        }
    </style>
</head>

<body>";


echo "<form id='body' method='post' action='" . htmlspecialchars($_SERVER["PHP_SELF"]) . "'>";
$msg = array();
$msg["Name"] = "<br>";
$msg["Gender"] = "<br>";
$msg["Faculty"] = "<br>";
$msg["DateBirth"] = "<br>";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["Name"])) {
        $msg["Name"] = "<span class='error'>Hãy nhập tên.</span>
            <br>";
    }
    if (empty($_POST["Gender"])) {
        $msg["Gender"] = "<span class='error'>Hãy chọn giới tính.</span>
            <br>";
    }
    if (empty($_POST["Faculty"])) {
        $msg["Faculty"] = "<span class='error'>Hãy chọn phân khoa.</span>
            <br>";
    }
    if (empty($_POST["DateBirth"])) {
        $msg["DateBirth"] = "<span class='error'>Hãy nhập ngày sinh.</span>
            <br>";
    }
}
echo $msg["Name"];
echo $msg["Gender"];
echo $msg["Faculty"];
echo $msg["DateBirth"];
echo "
        
        <table>
            <tr>
                <td class='background_blue'><label class='required'> Họ và tên </label> </td>
                <td> <input type='text' class='frames' style=' width: 150%;' name='Name' value='";
echo isset($_POST['Name']) ? $_POST['Name'] : '';
echo "'></td>
            </tr>

            <tr>
                <td class='background_blue'> <label class='required'> Giới tính </label> </td>
                <td>";
$gender = array(0 => 'Nam', 1 => 'Nữ');
for ($i = 0; $i < count($gender); $i++) {
    echo "<input type='radio'  name='Gender' value='" . $i . "'";
    echo isset($_POST['Gender']) && $_POST['Gender'] == $i ? " checked" : "";
    echo "/> " . $gender[$i];
}
echo "</td>
            </tr>
            <tr>
                <td class='background_blue'>
                <label class='required'> Phân Khoa </label>
                </td>
                <td> <select class='blue-box' name='Faculty'>";
$faculty = array('' => '', 'MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
foreach ($faculty as $key => $value) {
    echo " <option ";
    echo isset($_POST['Faculty']) && $_POST['Faculty'] == $key ? "selected " : "";
    echo "value ='" . $key . "'>" . $value . "</option> ";
}
echo "</td>
                </td>
            </tr>
            <tr> 
                <td class='background_blue'> <label class='required'> Ngày sinh </label></td>
                <td> <input type='date' class='blue-box' name='DateBirth' placeholder='dd-mm-yyyy' value='";
echo isset($_POST['DateBirth']) ? $_POST['DateBirth'] : "";
echo "'> </td>
            </tr>

            <tr>
                <td class='background_blue'> Địa chỉ </td>
                <td> <input type='text' class='frames' style=' width: 150%;' name='Address' value='";
echo isset($_POST['Address']) ? $_POST['Address'] : "";
echo "'></td>
            </tr>
        </table>
        <input type='submit' name='submit' value='Đăng kí' class='submit'>
    </form>

</body>

</html>";